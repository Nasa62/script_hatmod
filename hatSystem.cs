// Mr. Doom's HatMod Redux
// A redux of SimpleWell_HatMod with some feature ideas from RustyMunk_HatMod which are both old, dead, & crap.
//
// Code Sources:
// ScatteredSpace's Blockland City by, oh look, me! (and some friends)
// SimpleWell_HatMod by, oh look, still me!
// SimpleDB by, oh look, yep, me!
// Simple_FileLoader, again, by me...
// AlphaDB, yep me...
// AlphaLinkedList, still me......
// If you actually read that, I feel really sorry for you. Oh wait, no I don't.
//

function HatMod::onAdd(%this)
{
	activatePackage(HatMod);

	%this.database = new ScriptObject()
	{
		class = Alpha_DB;
		name = "hatMod";
		path = "server";
	}; //TODO: Handle if AlphaDB died.

}

function HatMod::onRemove(%this)
{
	deactivatePackage(HatMod);
}

function HatMod::addHat(%this, %hat)
{
	//Add hats, duh
}

function HatMod::tick(%this)
{
	//Do distribution
	
	//Can also call an idle check here to see if they have moved and if not don't grant them hat.
	
	%this.schedule($Pref::Server::HatMod::Tickrate, "tick");
}

function HatMod::isClientIdle(%this, %client)
{
	if($Pref::Server::HatMod::AntiIdle == false)
		return false;

	//TODO: Make sure client actually has a player and is alive.
	if(%client.player.position $= %this.playerIdlePosition[%client.BL_ID])
		return true;

	// Update position
	%this.playerIdlePosition[%client.BL_ID] = %client.player.position;
	
	return false;
}

package HatMod
{
	function GameConnection::onClientEnterGame(%client)
	{
		parent::onClientEnterGame(%client);

		if(!HatMod.database.isRow(%client.BL_ID))
			HatMod.database.addRow(%client.BL_ID);

		HatMod.playerIdlePosition[%client.BL_ID] = %client.player.position; //TODO: Make sure player exists.
	}
};
