// HAT DATABLOCK EXAMPLE

HatMod.addHat(SWMinerImage);

datablock ShapeBaseImageData(SWMinerImage)
{
	hatMod_uuid = "SWMiner"
	hatMod_name = "Mining Helmet";
	hatMod_rarity = 10; // 1-10, 1 is rarest.

	shapeFile = "./SHAPES/Miner.dts";
	emap = true;
	mountPoint = $HeadSlot;
	offset = "0 0.02 0.3";
	eyeOffset = "0 0 0.4";
	rotation = eulerToMatrix("0 0 0");
	scale = "0.1 0.1 0.1";
	doColorShift = false;
	colorShiftColor = "0.392 0.196 0.0 1.000";
};
