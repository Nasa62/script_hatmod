$Pref::Server::HatMod::SandboxMode = false;
$Pref::Server::HatMod::Tickrate = 3600000; // Millseconds, default is 3600000 (1 hour).
$Pref::Server::HatMod::AntiIdle = true; // If true, hats are not granted to idle users (they are not kicked like the old AFK kicker in SimpleWell_HatMod)
$Pref::Server::HatMod::IdleTime = 3000000; // Miliseconds, default is 3000000 (5 minutes)

$Pref::Server::HatMod::RandomItemPrefixes = "Vintage	Strange	Radiant	Doomy	Mr.	Shiny	Shiny	Shiny	Shiny	Shiny	Shiny";
