function LoadFiles(%path)
{
	for(%i=0;TRUE;%i++)
	{
		if(%i == 1)
			%file = findFirstFile(%path@"*.cs");
		else
			%file = findNextFile(%path@"*.cs");
		if(%file $= "") break;
		if(stripos(%LOADED,%file) >= 0) continue;
		%LOADED = TRIM(%LOADED TAB %FILE);
		exec(%file);
	}
}
